export const blue = {

  '--primary' : '#4717F6',
  '--primary-variant': '#000000',

  '--secondary': '#FFCA28',
  '--secondary-variant': '#FFF350',

  '--background': '#0E0B16',
  '--surface': '#171717',
  '--dialog': '#1E392A',
  '--cancel': '#FFCA28',

  '--on-primary': '#FFFFFF',
  '--on-secondary': '#000000',
  '--on-background': '#FFFFFF',
  '--on-surface': '#FFFFFF',
  '--on-cancel': '#000000',

  '--green': '#003300',
  '--red': '#8F0E00',
  '--yellow': '#FFD54F',
  '--blue': '#3f51b5',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#EEEEEE',
  '--black': '#212121'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Void',
      'de': 'Tiefe'
    },
    'description': {
      'en': 'Fall deep into the Void. Contrast compliant with WCAG AA',
      'de': 'Lass dich in die Tiefe fallen. Kontrast nach WCAG AA'
    }
  },
  'isDark': true,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};
